class StockPrice < ActiveRecord::Base

  belongs_to :stock, inverse_of: :stock_prices

  monetize :closing_price_cents

  validates :stock,               presence: true
  validates :date,                presence: true
  validates :closing_price_cents, presence: true, numericality: true


  def self.build_random_data
    closing_price = random_price
    (1..30).inject([]) do |acc, n|
      # missing day
      next acc if 0 == rand(5)

      acc << StockPrice.new(date: n.days.ago, closing_price_cents: closing_price)
      closing_price = random_price(closing_price)
      acc
    end
  end

private

  def self.random_price(last_value=nil)
    return rand(1000..9999) unless last_value
    # Try to not having a big gap between the 2 values
    rand((last_value - 1000)..(last_value + 1000))
  end
end

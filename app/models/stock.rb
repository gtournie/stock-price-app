class Stock < ActiveRecord::Base

  has_many :stock_prices, -> { order(:date) }, inverse_of: :stock, dependent: :destroy

  validates :name, presence: true

  def self.create_random_data!
    (1..4).map do |_|
      Stock.create name: random_name, stock_prices: StockPrice.build_random_data
    end
  end

private

  def self.random_name(size=4)
    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    (1..size).inject('') { |str, _| str << chars[rand * chars.length] }
  end
end

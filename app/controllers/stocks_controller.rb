class StocksController < ApplicationController

  helper_method :stocks

  def index
  end
  
  def create_data
    Stock.destroy_all
    Stock.create_random_data!
    respond_to do |format|
      format.js do
        render json: stocks.pluck(:id, :name)
      end
    end
  end
  
  def data
    stock = Stock.find(params[:id])
    respond_to do |format|
      format.js do
        data = stock.stock_prices.map { |sp| [sp.date.rfc2822, sp.closing_price.to_f] }
        render json: data
      end
    end
  end
  
private

  def stocks
    @stocks ||= Stock.order(:name)
  end
end

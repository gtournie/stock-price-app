class CreateStockPrices < ActiveRecord::Migration
  def change
    create_table :stock_prices do |t|
      t.integer  :stock_id,      null: false
      t.date     :date,          null: false
      t.monetize :closing_price, null: false
      t.timestamps               null: false
    end
    add_index :stock_prices, :stock_id
  end
end
